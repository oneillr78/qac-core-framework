package qac.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import com.beust.jcommander.Strings;



public class CommonActions {

	private static Logger log = Logger.getLogger(CommonActions.class);

	public static WebDriver driver;
	private static Properties properties;
	public static String ORFilePath = "C:\\QA\\OR.properties";
	public static String CommonFilePath = "CommonActions.properties";

	/*
	 * Retrieves values from property files for unique keys
	 * @param unique Key, path of the properties file
	 * 
	 * */
	public static String getPropertyValues(String path, String key) {
		String value = "";
		InputStream fis = null;
		try {
			if(path.equalsIgnoreCase(ORFilePath)){
				File f = new File(path);
				fis = new FileInputStream(f);				
			}else{
				fis = CommonActions.class.getClassLoader().getResourceAsStream(path);
			}	

			properties = new Properties(System.getProperties());
			properties.load(fis);
			if (Strings.isStringEmpty((String)properties.get(key))) {
				log.error("Value not found for key:[" +key+ "] in properties file[" +path+ "]");
				throw new Exception("Value not found for key:[" +key+ "] in properties file[" +path+ "]");
			}
			value = (String)properties.get(key);
			log.info("Value for key:[" +key+ "] in properties file[" +path+ "] is value["+value+"]");
		} catch(Exception e) {
			log.error("Error in getPropertyValues() :: "+e.getMessage());
		}
		return value;		
	}	

	/*
	 * Checks for alert pop-up and dismisses if any
	 * 
	 * */
	private static void handleAlert() {
		boolean flag = false;
		try {
			driver.switchTo().alert();
			flag = true;
		}catch(Exception e){
			flag = false;
			log.warn("No pop-up found:: method[handleAlert()] :: message["+e.getMessage()+"]");
		}
		if(flag){
			driver.switchTo().alert().dismiss();
			driver.switchTo().defaultContent();
		}
		log.info("Pop-up handled successfully");
	}


	/*
	 * initializes the preferred browser - default(Chrome)
	 * @param browser name, device name
	 * 
	 * */
	private static void initializeBrowser(String browserName, String deviceName) throws IOException {

		if (browserName.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", getPropertyValues(CommonFilePath,"chromeDriverPath"));

			if (!Strings.isStringEmpty(deviceName)) {
				Map<String, String> mobileEmulation = new HashMap<String, String>();
				mobileEmulation.put("deviceName", deviceName);

				Map<String, Object> chromeOptions = new HashMap<String, Object>();
				chromeOptions.put("mobileEmulation", mobileEmulation);
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

				driver = new ChromeDriver(capabilities);
				log.info("Launching browser with device["+deviceName+"] and browser["+browserName+"]");
			} else {

				driver = new ChromeDriver();

				log.info("Launching browser["+browserName+"]");
			}

		} else if (browserName.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
			log.info("Launching browser["+browserName+"]");

		} else if(browserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", getPropertyValues(CommonFilePath, "ieDriverPath"));
			driver = new InternetExplorerDriver();
			log.info("Launching browser["+browserName+"]");

		} else if (browserName.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
			log.info("Launching browser["+browserName+"]");

		} else {
			log.info("Browser not supported :: initializeBrowser");
		} 
	}


	/*
	 * Opens the preferred browser
	 * @param userInput.getBrowserName, userInput.getDeviceName
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean openBrowser() throws Throwable{
		boolean flag = false;
		try {			
			if (Strings.isStringEmpty(UserInput.getBrowserName())) {
				initializeBrowser(getPropertyValues(CommonFilePath, "defaultBrowser"), UserInput.getDeviceName());
			} else {
				initializeBrowser(UserInput.getBrowserName(),UserInput.getDeviceName());	
			}
			driver.get("http://qaconsultants.com/");
			flag =true;
			log.info("Launch status of QA-Consultants homepage :"+flag);
		} catch (Exception e) {
			log.error("OpenBrowser failed :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			return flag;	
		}
	}		


	/*
	 * Closes the browser tab
	 * */
	@SuppressWarnings("finally")
	public boolean closeBrowser() throws Throwable {
		boolean flag = false;
		try {
			driver.close();
			driver.quit();
			log.info("Browser tab closed successfully");
			flag = true;
		} catch (Exception e) {
			log.error("Error in closing browser :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			return flag;	
		}
	}

	/*
	 * Clicks web element(radio, check box, button)
	 * @param reference key
	 * */

	@SuppressWarnings("finally")
	public boolean click() throws Throwable {
		boolean flag = false;
		try {

			if(Strings.isStringEmpty(UserInput.getReferenceKey())) {
				log.error("No reference object mentioned");
			}else {
				String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());				
				log.info("Reference object located :: key["+UserInput.getReferenceKey()+"]");
				List<WebElement> weList = findWebElement(identifierExpression);
				WebElement we = weList.get(0);
				we.click();
				handleAlert();
				flag = true;
				log.info("Element click Successful :: "+flag);
			}	
		}catch(Exception e) {
			log.error("Error :: Closing browser tab. Error message["+e.getMessage()+"] LogCode200click");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("(LogCode201click) Element click Success :: "+flag);
			}
			return flag;	
		}
	}




	/*
	 * Selects option from drop down
	 * @param reference key, input value(select value from drop down list)
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean select() throws Throwable {
		boolean flag = false;
		try {

			if(Strings.isStringEmpty(UserInput.getReferenceKey())||Strings.isStringEmpty(UserInput.getInputValue())) {
				log.error("Missing data :: select");
			}else {
				String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
				String selectValue = UserInput.getInputValue();
				log.info(UserInput.getReferenceKey()+"[key] located");

				if (driver.getPageSource().contains(">"+selectValue)) {
					Select dropdown = new Select(findWebElement(identifierExpression).get(0));
					dropdown.selectByVisibleText(selectValue);
					handleAlert();
					flag = true;
					log.info("(LogCode302select) Element selection Success ::"+flag);
				} else {
					log.error(selectValue+ "is not a valid select option");
				}
			}
		}catch(Exception e) {
			log.error("Error in dropdown selection :: Closing browser :: error message["+e.getMessage()+"] LogCode300select");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Reason(LogCode301select) - Element selection Success ::"+flag);
			}
			return flag;
		}		
	}



	/*
	 * Provides input for any text box
	 * @param reference key, input value(input data)
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean input() throws Throwable {
		boolean flag = false;
		try {

			if (Strings.isStringEmpty(UserInput.getInputValue())) {
				log.error("No Input provided :: input");
			} else {
				if(Strings.isStringEmpty(UserInput.getReferenceKey())) {
					log.error("No reference object mentioned :: input");
				} else {
					String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
					List<WebElement> weList = findWebElement(identifierExpression);
					WebElement we = weList.get(0);
					if(!(identifierExpression.contains("date")&&identifierExpression.contains("picker"))){
						we.clear();
					}
					we.sendKeys(UserInput.getInputValue());
					flag = true;
					log.info("Located key["+UserInput.getReferenceKey()+"] and input data["+UserInput.getInputValue()+"]");
				}
			}
		}catch(Exception e) {
			log.error("Closing browser :: error message["+e.getMessage()+"] LogCode400input");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Reason (LogCode401input) - Input Success ::"+flag);
			}
			return flag;
		}

	}



	/*
	 * Navigates to the desired web site
	 * Web site name can be passed either as reference key or web site name
	 * Link can be passed either directly (direct URL starting with 'http' or by reference)
	 * @param getWebsiteName/getReferenceKey
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean navigate() throws Throwable {
		boolean flag = false;
		try {

			if(Strings.isStringEmpty(UserInput.getWebsiteName()) && Strings.isStringEmpty(UserInput.getReferenceKey())) {
				log.error("No website mentioned for navigation :: navigate()");
			}else {
				if (Strings.isStringEmpty(UserInput.getWebsiteName())){
					UserInput.setWebsiteName(UserInput.getReferenceKey());
					log.info("Initiating launch of referenced website["+UserInput.getWebsiteName()+"]...");
				}
				if(UserInput.getWebsiteName().substring(0, 4).equalsIgnoreCase("http")) {
					driver.navigate().to(UserInput.getWebsiteName());
					log.info("Launching website :: "+UserInput.getWebsiteName());
				} else {
					driver.navigate().to(getPropertyValues(ORFilePath, UserInput.getWebsiteName()));

					log.info("Launching website :: websiteKey["+UserInput.getWebsiteName()+"]");
				}
				flag = true;
				log.info("Website launch success ::"+flag);
			}
		}catch(Exception e) {
			log.error("Closing browser :: Error message["+e.getMessage()+"] LogCode500navigate");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Reason(LogCode501navigate) - Website launch success ::"+flag);
			}
			return flag;
		}
	}



	/*
	 * Clicks dynamic result populated as result of a web search
	 * Can be a radio/check box etc..
	 * @param userInput.getInputValue/userInput.getReferenceKey
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean clickDynamic() throws Throwable {

		boolean flag = false;
		try {

			int elementIndex = -1;
			String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
			List<WebElement> weList = findWebElement(identifierExpression);
			if (!(weList.size()== 0)) {
				if(Strings.isStringEmpty(UserInput.getInputValue())) {
					log.info("No preference from user :: selecting first option by default");
					elementIndex = 0;
				} else if(UserInput.getInputValue().equalsIgnoreCase("first")){
					elementIndex = 0;
				}else if(UserInput.getInputValue().equalsIgnoreCase("last")) {
					elementIndex = weList.size()-1;
				}else if(UserInput.getInputValue().equalsIgnoreCase("random")) {
					elementIndex = (int) weList.size()/2;
				}else {
					if (UserInput.getInputValue().matches("\\d+")) {
						int tempIndex = Integer.parseInt(UserInput.getInputValue());
						if (tempIndex > weList.size()){
							log.error("Result Position["+tempIndex+"] is invalid :: Total results found::"+weList.size());
							log.info("Selecting first option by default(LogCode103clickDynamicResult)");
							elementIndex = 0;
						} else if(tempIndex == 0){
							log.info("Result position 0 being considered as first result:: selecting first result");
							elementIndex = 0;
						} else {
							log.info("Selecting result["+tempIndex+"] from result set");
							elementIndex = tempIndex-1;
						}
					} else {
						log.error("Invalid preference["+UserInput.getInputValue()+"]");
						log.info("Selecting first option by default(LogCode102clickDynamicResult)");
					}
				}
				weList.get(elementIndex).click();
				handleAlert();
				flag = true;
			}else {
				log.error("No results found :: clickDynamicResult()");
			} 
		} catch(Exception e) {
			log.error("Closing browser :: Error message["+e.getMessage()+"] LogCode100clickDynamicResult");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Reason(LogCode101clickDynamicResult) - Result selection ::"+flag);
			}
			return flag;
		}

	}



	/*
	 * Takes screen shot and places at the preferred location
	 * @param - none
	 * 
	 * */
	public boolean screenShot(){
		boolean flag = false;
		try {
			String screenShotPath = getPropertyValues(CommonFilePath, "screenShotLocation");
			File file = new File(screenShotPath + System.currentTimeMillis() + ".jpg");
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, file);
			log.info("Screen Shot taken and placed at location["+screenShotPath+"]");
		}catch (Exception e) {
			log.error("Cannot take Screen Shot :: error message["+e.getMessage()+"]");
		}
		return flag;
	}



	/*
	 * validates particular web element
	 * and its content(if mentioned)
	 * @param - userInput.getReferenceKey / userInput.getInputValue
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean validate() throws Throwable {
		boolean flag = false;
		int time = 0;
		try{

			if (Strings.isStringEmpty(UserInput.getReferenceKey())) {
				throw new Exception("Reference for webelement to be validated not provided - Error Code::UN1006-A");
			} else {
				log.info("Invoking getDefaultWaitTime() - LogCode602");
				time = retrieveWaitTime();
				String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
				WebElement we = findWebElement(identifierExpression).get(0);
				(new WebDriverWait(driver, time))
				.until(ExpectedConditions.visibilityOf(we));
				log.info(UserInput.getReferenceKey()+" located :: "+"Successfully waited for:: seconds["+time+"] LogCode603wait");
				log.info("Element validation success :: element["+UserInput.getReferenceKey()+"]");
				if (Strings.isStringEmpty(UserInput.getInputValue())) {
					flag = true;
					log.info("No data to validate - LogCode604wait");
				} else {
					if (we.getText().equalsIgnoreCase(UserInput.getInputValue())) {
						flag = true;
						log.info("Data validated successfully - LogCode605");
					} else {
						log.info("Data Sent : "+UserInput.getInputValue());
						log.info("Original result : "+we.getText());
						log.error("Data validation failed - LogCode601:Trace-6A");
					}
				}
			}
		}catch(Exception e){
			log.error("Closing browser :: Expected WebElement cannot be found - LogCode600 :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Expected WebElement cannot be found - LogCode601 :: Success-status["+flag+"]");
			}
			return flag;
		}		
	}	

	/*
	 * Executes a .exe file passed as input
	 * or passed as ORProperty reference
	 * @param userInput.getInputValue / userInput.getReferenceKey
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean execute() throws Throwable{
		boolean flag = false;
		String exeFile = "";
		try {

			if(Strings.isStringEmpty(UserInput.getInputValue())) {
				if (Strings.isStringEmpty(UserInput.getReferenceKey())) {
					throw new Exception("No file mentioned LogCode800:Trace-A");
				} else {
					exeFile = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
					log.info("Retrieval of exec file path Success - filePath["+exeFile+"] LogCode802");
				}
			} else {
				exeFile = UserInput.getInputValue();
				log.info("Retrieval of exec file path Success - filePath["+exeFile+"] LogCode803");
			}
			if (!exeFile.equalsIgnoreCase("") &&
					exeFile.endsWith(".exe")) {
				Runtime.getRuntime().exec(exeFile);
				flag = true;
				log.info("Successfully executed exec file at path["+exeFile+"] LogCode804");
			} else {
				log.error("File name empty or incorrect format - LogCode805");
			}
		}catch(Exception e) {
			log.error("Closing browser :: Execution error - LogCode800 :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Execution error - LogCode801 :: Success-status["+flag+"]");
			}
			return flag;
		}
	}


	/*
	 * Customized validation for loading of web image
	 * @param userInput.getReferenceKey
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean validateImage() throws Throwable{
		boolean flag = false;
		try {
			//			if (userInput == null) {
			//				throw new Exception("UserInput is Null - Error Code::UN1009");
			//			}
			if (!Strings.isStringEmpty(UserInput.getReferenceKey())) {
				String identifierExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());
				log.info("Reference object located :: key["+UserInput.getReferenceKey()+"]");
				List<WebElement> weList = findWebElement(identifierExpression);
				if(weList.size()!= 1) {
					throw new Exception("Identifier is not unique. Identifier["+identifierExpression+"] being used by "+weList.size()+" - webelements");
				} 
				WebElement we = weList.get(0);

				Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete &&"
						+ " typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", we);
				if (!ImagePresent) {
					log.error("Image display Status["+ImagePresent+"] LogCode801-TraceA");
				}
				else {
					log.info("Image display Status["+ImagePresent+"] LogCode802");
					flag = true;
				}
			} else {
				log.error("No reference provided for image location - LogCode801-TraceB");
			}

		}catch(Exception e) {
			log.error("Closing browser :: Image validation failed - LogCode900 :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Image validation failed - LogCode901 :: Success-status["+flag+"]");
			}
			return flag;
		}


	}

	/*
	 * Customized testing for
	 * functionalities of web video
	 * @param userInput.getReferenceKey / userInput.getWaitTime / userInput.getInputValue
	 * 
	 * */
	@SuppressWarnings("finally")
	public boolean testVideo () throws Throwable {
		boolean flag = false;
		try {
			//			if (userInput == null) {
			//				throw new Exception("UserInput is Null - Error Code::UN1010");
			//			}
			if (Strings.isStringEmpty(UserInput.getReferenceKey())) {
				throw new Exception("No reference provided for locating video");
			} else {
				log.info("Initiating validation for presence of video[web-element]");
				if(validate()) {
					log.info("Validation success!");

					int time = retrieveWaitTime();


					JavascriptExecutor js = (JavascriptExecutor) driver;
					String xpathExpression = getPropertyValues(ORFilePath, UserInput.getReferenceKey());

					//Play Video
					log.info("Play Video");
					js .executeScript("document.getElementByXpath("+xpathExpression+").play()");
					driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

					//Pause playing video
					log.info("Pause Video");
					js .executeScript("document.getElementByXpath("+xpathExpression+").pause()");
					//Check if video has been paused
					log.info("Video Pause attempted. Status["+
							js .executeScript("document.getElementByXpath("+xpathExpression+").paused")
					+"]");

					//Resume video play
					log.info("Play Video again");
					js .executeScript("document.getElementByXpath("+xpathExpression+").play()");

					//Play video from start
					log.info("Play video from start");
					js .executeScript("document.getElementByXpath("+xpathExpression+").currentTime=0");
					driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);

					//Reload video
					log.info("Reload video");
					js .executeScript("document.getElementByXpath("+xpathExpression+").load()");

					log.info("End of video test");
					flag = true;
				}
			}

		}catch(Exception e) {
			log.error("Closing browser :: Video validation failed - LogCode1010 :: error message["+e.getMessage()+"]");
			throw e;
		}
		finally {
			if (!flag) {
				driver.close();
				log.error("Closing browser :: Video validation failed - LogCode1011 :: Success-status["+flag+"]");
			}
			return flag;
		}
	}

	/*
	 * If time not specified as input then
	 * reads defaultWaitTime from common properties
	 * if time is invalid returns default time as
	 * 5 seconds
	 * @param - /userInput.getWaitTime/userInput.getInputValue/commonProperties.defaultWaitTime
	 * 
	 * */
	@SuppressWarnings("finally")
	private int retrieveWaitTime() {
		int time = 0;
		try {
			if (!(UserInput.getWaitTime() == 0)) {
				time = UserInput.getWaitTime();
				log.info("wait time retrieved from getWaitTime :: seconds["+time+"]");
			} else {
				if (!Strings.isStringEmpty(UserInput.getInputValue()) && UserInput.getInputValue().matches("//d+")) {
					time = Integer.parseInt(UserInput.getInputValue());
					log.info("wait time retrieved from getInputValue :: seconds["+time+"]");
				} else {
					if(!Strings.isStringEmpty(getPropertyValues(CommonFilePath, "defaultWaitTime"))) {
						log.info("Retrieving default time from common properties.");
						String tempTime = getPropertyValues(CommonFilePath, "defaultWaitTime");
						if (tempTime.matches("\\d+")) {
							time = Integer.parseInt(tempTime);
							log.info("Setting wait time from common properties :: seconds["+time+"]");
						}else {
							log.warn("Invalid format for timer :: seconds["+tempTime+"] is invalid.");
						}
					} else {
						log.warn("No time specified in common properties. Timer set to seconds["+time+"]");
					}
				}
			}			
		}catch(Exception e) {
			log.warn("Error encountered in getDefaultWaitTime() -- error message["+e.getMessage()+"]");
		}
		finally {
			if (time == 0) {
				log.info("Timer was set to seconds[0]. Hence, setting time as 5 seconds - [DEFAULT WAIT TIME]");
				time = 5;
			}
			log.info("Returning time as seconds["+time+"]");
			return time;
		}
	}

	// *** THIS METHOD WAS ADDED FOR EXPECTED OUTPUT COLUMN ***
	public boolean verifyResult() {
		boolean flag = false;
		try {
			String xpath = getPropertyValues(ORFilePath, UserInput.getReferenceKey());

			Thread.sleep(1500);
			String actualresult = driver.findElement(By.xpath(xpath)).getText();
			Thread.sleep(500);
			String Result = UserInput.getExpectedOutputValue();	

			if (Result != "" && Result != null) {

				if (actualresult.contains(Result)) {
					log.info("The " + actualresult + " output is as expected.");
					flag = true;
				}				
			}

		} catch (Exception e) {
			log.error("Not able to verify --- " + e.getMessage());
			flag = false;
		}

		return flag;
	}



	/*
	 * ******* Code for finding web-elements starts here ******************************************
	 * It is now dynamic and can handle any type of identifier viz. Xpath/cssSelector/id/name etc.
	 * ********************************************************************************************/

	private List<WebElement> findWebElement(String identifier) {
		List<WebElement> we = null;
		try {
			we = tryXpathForList(identifier);
			if(we == null || we.isEmpty()){
				throw new Exception("Unable to locate web element.["+we.size()+" elements found]");
			}else if(we.size() == 1) {
				log.info("Unique element located");
			}else {
				throw new Exception("Element is not unique. Number of web-elements with same identifier is :: "+we.size());
			}
		}catch(Exception e) {
			log.error("findWebElement :: "+e.getMessage());
		}
		return we;
	}
	private List<WebElement> tryXpathForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.xpath(identifier));
		}catch(Exception e) {
			log.error("tryXpathForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryCssSelectorForList(identifier);
		}else {
			log.info("Web element identified by xpath");
		}
		return we;
	}
	private List<WebElement> tryCssSelectorForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.cssSelector(identifier));
		}catch(Exception e) {
			log.error("tryCssSelectorForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryClassNameForList(identifier);
		}else {
			log.info("Web element identified by cssSelector");
		}
		return we;
	}
	private List<WebElement> tryClassNameForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.className(identifier));
		}catch(Exception e) {
			log.error("tryClassNameForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryIdForList(identifier);
		}else {
			log.info("Web element identified by className");
		}
		return we;
	}
	private List<WebElement> tryIdForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.id(identifier));
		}catch(Exception e) {
			log.error("tryIdForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryNameForList(identifier);
		}else {
			log.info("Web element identified by id");
		}
		return we;
	}
	private List<WebElement> tryNameForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.name(identifier));
		}catch(Exception e) {
			log.warn("tryNameForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryTagNameForList(identifier);
		}else {
			log.info("Web element identified by name");
		}
		return we;
	}
	private List<WebElement> tryTagNameForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.tagName(identifier));
		}catch(Exception e) {
			log.error("tryTagNameForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryLinkTextForList(identifier);
		}else {
			log.info("Web element identified by tagName");
		}
		return we;
	}
	private List<WebElement> tryLinkTextForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.linkText(identifier));
		}catch(Exception e) {
			log.error("tryLinkTextForList :: "+e.getMessage());
		}
		if(we.isEmpty()) {
			we = tryPartialLinkTextForList(identifier);
		}else {
			log.info("Web element identified by linkText");
		}
		return we;
	}
	private List<WebElement> tryPartialLinkTextForList(String identifier) {
		List<WebElement> we = null;
		try {
			we = driver.findElements(By.partialLinkText(identifier));
		}catch(Exception e) {
			log.warn("tryPartialLinkTextForList :: "+e.getMessage());
			return null;
		}
		if(!we.isEmpty()) {
			log.info("Web element identified by partialLinkText");
		}
		return we;
	}

	/*
	 * ********** Code for finding web-elements ends here *******
	 * **********************************************************/

}
