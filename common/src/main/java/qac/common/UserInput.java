package qac.common;

public class UserInput {
	private static String inputValue;
	private static String referenceKey;
	private static String browserName;
	private static String websiteName;
	private static String deviceName;
	private static int waitTime;
	private static String expectedOutputValue;
	public static String getInputValue() {
		return inputValue;
	}
	public static void setInputValue(String inputValue) {
		UserInput.inputValue = inputValue;
	}
	public static String getReferenceKey() {
		return referenceKey;
	}
	public static void setReferenceKey(String referenceKey) {
		UserInput.referenceKey = referenceKey;
	}
	public static String getBrowserName() {
		return browserName;
	}
	public static void setBrowserName(String browserName) {
		UserInput.browserName = browserName;
	}
	public static String getWebsiteName() {
		return websiteName;
	}
	public static void setWebsiteName(String websiteName) {
		UserInput.websiteName = websiteName;
	}
	public static String getDeviceName() {
		return deviceName;
	}
	public static void setDeviceName(String deviceName) {
		UserInput.deviceName = deviceName;
	}
	public static int getWaitTime() {
		return waitTime;
	}
	public static void setWaitTime(int waitTime) {
		UserInput.waitTime = waitTime;
	}
	public static String getExpectedOutputValue() {
		return expectedOutputValue;
	}
	public static void setExpectedOutputValue(String expectedOutputValue) {
		UserInput.expectedOutputValue = expectedOutputValue;
	}
	
	

}
